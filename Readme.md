# GitLab CI: Cache
([Source](https://blog.eleven-labs.com/fr/introduction-gitlab-ci/))

---

## CACHE

Cette directive permet de jouer avec du cache. Le cache est intéressant pour spécifier une liste de fichiers et de répertoires à mettre en cache tout le long de votre pipeline. Une fois la pipeline terminée le cache sera détruit.

Plusieurs sous-directives sont possibles :

- *paths* : obligatoire, elle permet de spécifier la liste de fichiers et/ou répertoires à mettre en cache
- *key* : facultative, elle permet de définir une clé pour la liste de fichiers et/ou de répertoires. Personnellement je n’en ai toujours pas vu l’utilité.- - 
- *untracked* : facultative, elle permet de spécifier que les fichiers ne doivent pas être suivis par votre dépôt git en cas d’un push lors de votre pipeline.
- *policy* : facultative, elle permet de dire que le cache doit être récupéré ou sauvegardé lors d’un job (push ou pull).
``` yaml
stages:
- build

cache:
  key: build-cache
  paths:
    - vendor/
    
before_script:
- echo "In before script ..."
- mkdir vendor/
- echo "build" > vendor/hello.txt

job A:
  stage: build
  script:
  - echo "In the job ...."
  - cat vendor/hello.txt
  cache:
    key: build-cache
``` 

### Caches vs Artifacts  [Ref.](https://docs.gitlab.com/ee/ci/caching/#cache-vs-artifacts)
 

  > **cache** - Use for temporary storage for project dependencies. Not useful for keeping intermediate build results, like jar or apk files. Cache was designed to be used to speed up invocations of subsequent runs of a given job, by keeping things like dependencies (e.g., npm packages, Go vendor packages, etc.) so they don't have to be re-fetched from the public internet. While the cache can be abused to pass intermediate build results between stages, there may be cases where artifacts are a better fit.


>**artifacts** - Use for stage results that will be passed between stages. Artifacts were designed to upload some compiled/generated bits of the build, and they can be fetched by any number of concurrent Runners. They are guaranteed to be available and are there to pass data between jobs. They are also exposed to be downloaded from the UI.)